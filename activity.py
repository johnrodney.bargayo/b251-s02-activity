#1. Accept a year input from the user and determine if it is a leap year or not.

year=int(input("Enter year to be checked:"))
if(year%4==0 and year%100!=0 or year%400==0):
    print("The year is a leap year!")
elif year > 0:
    print("The year isn't a leap year!")
else:
	print("The inputed number is negative")

#2. Accept two numbers number (row and col) from the user and create a grid of asterisk using the two numbers (row and col)

rows=int(input("Enter number of rows:\n"))
column=int(input("Enter a number of colums:\n"))
for x in range(rows):
    for y in range(column):
        print('*',end = ' ')
    print()